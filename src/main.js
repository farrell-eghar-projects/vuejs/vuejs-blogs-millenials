import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios' 
import Toasted from 'vue-toasted';
import { BootstrapVue } from 'bootstrap-vue'
import AxiosPlugin from 'vue-axios-cors';
import swal from 'sweetalert';

Vue.use(AxiosPlugin)

Vue.use(Toasted)
Vue.use(BootstrapVue)

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// axios.defaults.headers.common["Authorization"] = 'Bearer ' + localStorage.getItem("token");
// axios.defaults.headers.post["Content-Type"] = "application/json";

// store.dispatch('auth/me').finally(() => {
//   new Vue({
//     router,
//     store,
//     axios,
//     swal,
//     render: h => h(App)
//   }).$mount('#app')
// })



new Vue({
  router,
  store,
  axios,
  swal,
  render: h => h(App)
}).$mount('#app')

Vue.config.productionTip = false
